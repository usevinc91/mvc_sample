﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using mvc_sample.Models;

namespace mvc_sample.ViewModels
{
    public class RandomMovieViewModel
    {
        public Movie Movie { get; set; }
        public List<Customer> Customers { get; set; }
    }
}